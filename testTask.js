
class AnimalES6 {
    constructor(name){
        this.name = name;
    }

    /**
     * Method to get animal name
     *
     * @returns str
     * @memberof Animal
     */
    getName(){
        return this.name;
    }
}

class DogES6 extends AnimalES6 {
    constructor(dogName){
        super(dogName);
    }
    /**
     * Method to say dog is barking
     *
     * @returns str
     * @memberof Dog
     */
    bark(){
        return `Dog ${this.getName()} is barking`;
    }
}



let dogES6 = new DogES6("Alabay");//create new object of the dog 
console.log("test getName", dogES6.getName);
console.log("test bark", dogES6.bark);

//ES5
/**
 * contructor of the Animal class written on prototypes
 *
 * @param {str} name - animal name
 */
function AnimalES5( name ) {
    this.name = name;
}

/**
    * Method to get animal name
    * @memberof Animal 
*/
AnimalES5.prototype.getName = function(){
    return this.name;
}

/**
 * constructor of the Dog class written on prototypes
 *
 * @param {str} dogName
 */
function DogES5( dogName ){
    this._super.call(this, dogName);//calls contructor of the Animal class
}

DogES5.prototype = Object.create(AnimalES5.prototype);//extends Dog from Animal

DogES5.prototype.constructor = DogES5;//Set constructor back to Dog class

DogES5.prototype._super = AnimalES5;//Declares _super connected to Animal

/** 
 * Method to send str "Dog {dog name} is barking";
 * @memberof Dog
*/
DogES5.prototype.bark = function(){
    return "Dog "+this.getName()+" is barking";
}

//testing our classes
var dogES5 = new DogES5("alabay");

console.log(dogES5.getName());
console.log(dogES5.bark())